// Brikrompisto - SFML 2 - Rachel J. Morris - Moosader.com

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <vector>
#include <iostream>
#include <string>
#include <sstream>

#include "Ludeto.h"
#include "Pilko.h"
#include "Briko.h"

void KreasBrikojn( std::vector< Briko >& brikoj );
bool CxuKolizio( sf::Vector2f poz1, sf::Vector2f dim1, sf::Vector2f poz2, sf::Vector2f dim2 );
std::string IntToString( int num );

int main()
{
    sf::Vector2i ekrandistingivo( 800, 600 );
    sf::RenderWindow fenestro( sf::VideoMode( ekrandistingivo.x, ekrandistingivo.y ), "BRIKROMPISTO" );

    // background
    sf::RectangleShape fono( sf::Vector2f( ekrandistingivo.x, ekrandistingivo.y ) );
    fono.setFillColor( sf::Color( 100, 100, 100, 255 ) );

    Ludeto ludeto;
    Pilko pilko;
    pilko.AtribuiPozicio( ludeto.PreniPozicio(), ludeto.PreniDimensio() );

    std::vector< Briko > brikoj;
    KreasBrikojn( brikoj );

    sf::Font tiparo;
    tiparo.loadFromFile( "fonts/Averia-Bold.ttf" );

    sf::Text poentaro;
    poentaro.setFont( tiparo );
    poentaro.setPosition( 10, 10 );
    poentaro.setColor( sf::Color::White );
    poentaro.setCharacterSize( 18 );

    bool pauxzo = true;
    bool finludo = false;

    while ( fenestro.isOpen() )
    {
        // Klavaro
        if ( sf::Keyboard::isKeyPressed( sf::Keyboard::F4 ) && sf::Keyboard::isKeyPressed( sf::Keyboard::LAlt ) )
        {
            fenestro.close();
        }

        if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Left ) ||
             sf::Keyboard::isKeyPressed( sf::Keyboard::A ))
        {
            ludeto.Transloki( MALDEKSTRA );
            pauxzo = false;
        }
        else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Right ) ||
             sf::Keyboard::isKeyPressed( sf::Keyboard::D ))
        {
            ludeto.Transloki( DEKSTRA );
            pauxzo = false;
        }

        if ( pauxzo == false )
        {
            pilko.Gxisdatigo();
        }

        if ( CxuKolizio( ludeto.PreniPozicio(), ludeto.PreniDimensio(),
                        pilko.PreniPozicio(), pilko.PreniDimensio() ) )
        {
            Direkto batDirekto = CENTRA;

            if ( ludeto.PreniCentra() + 10 < pilko.PreniCentra() )
            {
                batDirekto = DEKSTRA;
            }
            else if ( ludeto.PreniCentra() - 10 > pilko.PreniCentra() )
            {
                batDirekto = MALDEKSTRA;
            }

            pilko.Resalti( batDirekto );
        }

        std::vector< int > elpreni;

        for ( unsigned int i = 0; i < brikoj.size(); i++ )
        {
            if ( CxuKolizio( brikoj[i].PreniPozicio(), brikoj[i].PreniDimensio(),
                        pilko.PreniPozicio(), pilko.PreniDimensio() ) )
            {
                Direkto batDirekto = CENTRA;

                if ( brikoj[i].PreniCentra() + 10 < pilko.PreniCentra() )
                {
                    batDirekto = DEKSTRA;
                }
                else if ( brikoj[i].PreniCentra() - 10 > pilko.PreniCentra() )
                {
                    batDirekto = MALDEKSTRA;
                }

                pilko.Resalti( batDirekto );
                elpreni.push_back( i );
                ludeto.AldoniPoentaro();
            }
        }

        for ( unsigned int i = 0; i < elpreni.size(); i++ )
        {
            brikoj.erase( brikoj.begin() + elpreni[i] );
        }

        if ( brikoj.size() == 0 )
        {
            finludo = true;
        }

        if ( finludo )
        {
            poentaro.setString( "FINLUDO! " + IntToString( ludeto.PreniPoentaroj() ) );
        }
        else
        {
            poentaro.setString( "Poentaroj: " + IntToString( ludeto.PreniPoentaroj() ) );
        }

        // Desegni
        fenestro.clear();
        fenestro.draw( fono );

        for ( unsigned int i = 0; i < brikoj.size(); i++ )
        {
            brikoj[i].Desegni( fenestro );
        }

        ludeto.Desegni( fenestro );
        pilko.Desegni( fenestro );

        fenestro.draw( poentaro );

        fenestro.display();
    }

    return 0;
}

void KreasBrikojn( std::vector< Briko >& brikoj )
{
    int kvanto = 0;
    for ( int y = 0; y < 3; y++ )
    {
        for ( int x = 0; x < 800 / 55; x++ )
        {
            Briko briko( sf::Vector2f( x * 55 + 15, y * 30 + 50 ), kvanto );
            brikoj.push_back( briko );
            kvanto++;
        }
    }
}

bool CxuKolizio( sf::Vector2f poz1, sf::Vector2f dim1, sf::Vector2f poz2, sf::Vector2f dim2 )
{
    return (    poz1.x < poz2.x + dim2.x &&
                poz1.x + dim1.x > poz2.x &&
                poz1.y < poz2.y + dim2.y &&
                poz1.y + dim1.y > poz2.y );
}

std::string IntToString( int num )
{
    std::stringstream ss;
    ss << num;
    return ss.str();
}
