#include "Briko.h"

Briko::Briko( sf::Vector2f poz, int index )
{
    m_pozicio = poz;
    m_dimensio.x = 50;
    m_dimensio.y = 25;

    if ( index % 4 == 0 )
    {
        m_kolora = sf::Color( 255, 100, 100, 255 );
    }
    else if ( index % 4 == 1 )
    {
        m_kolora = sf::Color( 100, 255, 100, 255 );
    }
    else if ( index % 4 == 2 )
    {
        m_kolora = sf::Color( 100, 100, 255, 255 );
    }
    else if ( index % 4 == 3 )
    {
        m_kolora = sf::Color( 255, 100, 255, 255 );
    }
}

void Briko::Desegni( sf::RenderWindow& fenestro )
{
    sf::RectangleShape formo( sf::Vector2f( m_dimensio.x, m_dimensio.y ) );
    formo.setFillColor( m_kolora );
    formo.setPosition( sf::Vector2f( m_pozicio.x, m_pozicio.y ) );
    formo.setOutlineThickness( 2 );
    formo.setOutlineColor( sf::Color( 0, 0, 0, 255 ) );

    fenestro.draw( formo );
}

sf::Vector2f Briko::PreniPozicio()
{
    return m_pozicio;
}

sf::Vector2f Briko::PreniDimensio()
{
    return m_dimensio;
}

float Briko::PreniCentra()
{
    return m_pozicio.x + m_dimensio.x / 2;
}
