#include "Ludeto.h"

Ludeto::Ludeto()
{
    m_dimensio.x = 100;
    m_dimensio.y = 25;
    m_pozicio.x = 800 / 2 - m_dimensio.x / 2;
    m_pozicio.y = 600 - m_dimensio.y * 2;
    m_rapido = 6.0f;
    m_poentaro = 0;
}

sf::Vector2f Ludeto::PreniPozicio()
{
    return m_pozicio;
}

sf::Vector2f Ludeto::PreniDimensio()
{
    return m_dimensio;
}

float Ludeto::PreniCentra()
{
    return m_pozicio.x + m_dimensio.x / 2;
}

int Ludeto::PreniPoentaroj()
{
    return m_poentaro;
}

void Ludeto::Desegni( sf::RenderWindow& fenestro )
{
    sf::RectangleShape formo( sf::Vector2f( m_dimensio.x, m_dimensio.y ) );
    formo.setFillColor( sf::Color( 255, 255, 255, 255 ) );
    formo.setPosition( sf::Vector2f( m_pozicio.x, m_pozicio.y ) );
    formo.setOutlineThickness( 2 );
    formo.setOutlineColor( sf::Color( 0, 0, 0, 255 ) );
    fenestro.draw( formo );
}

void Ludeto::Transloki( Direkto direkto )
{
    if ( direkto == MALDEKSTRA )
    {
        m_pozicio.x -= m_rapido;
    }
    else if ( direkto == DEKSTRA )
    {
        m_pozicio.x += m_rapido;
    }

    if ( m_pozicio.x < 0 ) { m_pozicio.x = 0; }
    if ( m_pozicio.x > 800 - m_dimensio.x ) { m_pozicio.x = 800 - m_dimensio.x; }
}

void Ludeto::AldoniPoentaro()
{
    m_poentaro += 1;
}
