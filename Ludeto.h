#ifndef _LUDETO
#define _LUDETO

#include <SFML/Graphics.hpp>

#include "Direkto.h"

class Ludeto
{
    private:
    sf::Vector2f m_pozicio;
    sf::Vector2f m_dimensio;
    float m_rapido;
    int m_poentaro;

    public:
    Ludeto();
    sf::Vector2f PreniPozicio();
    sf::Vector2f PreniDimensio();
    int PreniPoentaroj();
    float PreniCentra();
    void Desegni( sf::RenderWindow& fenestro );
    void Transloki( Direkto );
    void AldoniPoentaro();
};

#endif
