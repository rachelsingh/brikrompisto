#ifndef _BRIKO
#define _BRIKO

#include <SFML/Graphics.hpp>

class Briko
{
    private:
    sf::Vector2f m_pozicio;
    sf::Vector2f m_dimensio;
    sf::Color m_kolora;

    public:
    Briko( sf::Vector2f poz, int index );
    void Desegni( sf::RenderWindow& fenestro );
    sf::Vector2f PreniPozicio();
    sf::Vector2f PreniDimensio();
    float PreniCentra();
};

#endif
