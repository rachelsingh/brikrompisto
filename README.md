# Brikrompisto

![Screenshot](https://github.com/Esperanto-Arcade/Brikrompisto/blob/master/Screenshot.png?raw=true)

Rompu la Brikojn!

## Kiel ludi

1. [Elŝutu la ludon](https://github.com/Esperanto-Arcade/Teniso/archive/master.zip)
1. Elŝutu [SFML2](http://www.sfml-dev.org/) biblioteko kaj C++ kompililo
1. Kompilu la ludon

## Informo

Kreis de [Rachel J. Morris](http://www.moosader.com/)

## License

MIT License (Vidu la "LICENSE" dosiero)

## Esperanto Arcade

Retpaĝaro: [Esperanto Arcade sur GitHub](https://github.com/Esperanto-Arcade)

Kreu malfermitkodajn videoludojn!
