#ifndef _PILKO
#define _PILKO

#include <SFML/Graphics.hpp>

#include "Direkto.h"

class Pilko
{
    private:
    sf::Vector2f m_pozicio;
    float m_radiuso;
    sf::Vector2f m_rapido;
    float m_maksRapido;

    public:
    Pilko();
    void AtribuiPozicio( sf::Vector2f poz, sf::Vector2f dim );
    sf::Vector2f PreniPozicio();
    sf::Vector2f PreniDimensio();
    void Desegni( sf::RenderWindow& fenestro );
    void Gxisdatigo();
    void Resalti( Direkto dir );
    float PreniCentra();
};

#endif
