#include "Pilko.h"

Pilko::Pilko()
{
    m_rapido.x = 0;
    m_rapido.y = -3;
    m_radiuso = 10;
    m_maksRapido = 7;
}

void Pilko::AtribuiPozicio( sf::Vector2f poz, sf::Vector2f dim )
{
    m_pozicio.x = poz.x + dim.x / 2 - m_radiuso;
    m_pozicio.y = poz.y - m_radiuso * 2;
}

void Pilko::Desegni( sf::RenderWindow& fenestro )
{
    sf::CircleShape formo( m_radiuso );
    formo.setFillColor( sf::Color( 255, 255, 100, 255 ) );
    formo.setPosition( sf::Vector2f( m_pozicio.x, m_pozicio.y ) );
    formo.setOutlineThickness( 2 );
    formo.setOutlineColor( sf::Color( 0, 0, 0, 255 ) );
    fenestro.draw( formo );
}

void Pilko::Gxisdatigo()
{
    // transloki
    m_pozicio.x += m_rapido.x;
    m_pozicio.y += m_rapido.y;

    // resalti
    if ( m_pozicio.x < 0 || m_pozicio.x > 800 - m_radiuso * 2 )
    {
        m_rapido.x = -m_rapido.x;

        if ( m_rapido.x < 0 )
        {
            m_rapido.x -= 1;
        }
        else if ( m_rapido.x > 0 )
        {
            m_rapido.x += 1;
        }
    }

    if ( m_pozicio.y < 0 || m_pozicio.y > 600 - m_radiuso * 2 )
    {
        m_rapido.y = -m_rapido.y;

        if ( m_rapido.y < 0 )
        {
            m_rapido.y -= 1;
        }
        else if ( m_rapido.y > 0 )
        {
            m_rapido.y += 1;
        }
    }

    // maksimumo rapido
    if ( m_rapido.x > m_maksRapido )
    {
        m_rapido.x = m_maksRapido;
    }
    else if ( m_rapido.x < -m_maksRapido )
    {
        m_rapido.x = -m_maksRapido;
    }
    if ( m_rapido.y > m_maksRapido )
    {
        m_rapido.y = m_maksRapido;
    }
    else if ( m_rapido.y < -m_maksRapido )
    {
        m_rapido.y = -m_maksRapido;
    }
}

sf::Vector2f Pilko::PreniPozicio()
{
    return m_pozicio;
}

sf::Vector2f Pilko::PreniDimensio()
{
    return sf::Vector2f( m_radiuso * 2, m_radiuso * 2 );
}

void Pilko::Resalti( Direkto dir )
{
    m_rapido.y = -m_rapido.y;

    float rapido = m_rapido.y;
    if ( rapido < 0 ) { rapido = -rapido; }

    if ( dir == CENTRA )
    {
        m_rapido.x += ( -rapido / 2 );
    }

    else if ( dir == MALDEKSTRA )
    {
        m_rapido.x -= rapido;
    }

    else if ( dir == DEKSTRA )
    {
        m_rapido.x += rapido;
    }
}


float Pilko::PreniCentra()
{
    return m_pozicio.x + m_radiuso;
}
